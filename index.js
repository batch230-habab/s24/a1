let getCube = (numA) =>{
    console.log(`The cube of ${numA} is ${numA**3}`)
}

getCube(2);

let homeAddress = [258, "Washington Ave NW", "California", "90011"];
let [houseNum, streetName, state, zip] = homeAddress;
console.log(`I live at ${houseNum} ${streetName}, ${state} ${zip}`);

let animal = {
    assignedName: "Lolong",
    type: "saltwater crocodile",
    weight: 1075,
    length: '20 ft 3 in'
}

let {assignedName, type, weight, length} = animal
console.log(`${assignedName} was a ${type}. He weighed at ${weight} kgs with a measurement of ${length}.`);

let numArr=[1,2,3,4,5];
numArr.forEach(num=>console.log(num));

/* let totalOfArr = numArr.reduce(acc, curr) => {
    console.log
} */

let reduceNumber = () => {
    result = numArr.reduce((acc, curr) => acc + curr);
    console.log(result);
}
reduceNumber();

class Dog {
    constructor(dogName, dogAge, dogBreed){
        this.dogName = dogName;
        this.dogAge = dogAge;
        this.dogBreed = dogBreed;
    }
}

let dog1 = new Dog("Frankie", 5, "Miniature Dachsund");
console.log(dog1);